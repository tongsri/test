*** Settings ***
Resource          ../../../KeywordRedefine/KeywordCommon/PageKeyword/KeywordCommon.robot
Resource          ../PageVariable/ExVariables.robot
Resource          ../../../Config/LocalConfig.txt

*** Keywords ***
Switch To Context Ex
    [Arguments]    ${TimetoStartMainPage}
    [Documentation]    *Sleep for wait Mainpage app for is switch to context webview*
    Sleep    ${TimetoStartMainPage}
    Switch To Context    WEBVIEW_com.krungthai.corporateUAT    #WEBVIEW_unknown

Switch To Context Native
    [Documentation]    *Sleep for wait Mainpage app for is switch to context native*
    Switch To Context    NATIVE_APP    #NATIVE

ExLaunchApplication
    Open Application Android    ${lo_IPAppium}    ${lo_PlatformName}    ${lo_PlatformVersion}    ${lo_SerialNumber}    ${ExappPackage}    ${ExappActivity}
    ...    ${lo_NoReset}    ${newCommandTimeout}    ${nativeWebScreenshot}
    # Switch To Context Ex    3
    # Wait Mobile Until Page Contains Element    //*[@class="logo"]
    Sleep    3